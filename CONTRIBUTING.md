# Contribute to Silva

Community made patches, localizations, bug reports and other contributions are always welcome and are crucial to ensure that Silva becomes and remains a success.

When contributing, please ensure you follow the guidelines below so that we can keep on top of things.

## Getting Started

* Check to see if a [relevant issue](https://gitlab.com/pop-planet/silva/issues) already exists.

  * If an issue already exists, and you can provide relevant information, please do so.

  * Please refrain from posting "me too" comments, they just clutter things up.

* If no relevant issue exists, [submit a ticket](https://gitlab.com/pop-planet/silva/issues/new) for the issue.

  * Clearly describe the issue, including steps to reproduce the bug

  * Make sure to include what version you are using, what operating system/distro, and any other relevant information.

## Making Changes

* Fork the repository on GitLab.

* Make the changes to your forked repository.

* When committing, reference the relevant issue (if one exists) and include a note about the fix.

* Push the changes to your fork and submit a pull request to the 'master' branch of the Silva repository.

## Documentation

* We strive to ensure that Silva is well documented.

  * If you add or significantly change something, please ensure you include relevant inline documentation.

At this point you're waiting for your pull request to be merged!
